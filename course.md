# Suche im TIB-Portal

Herzlich Willkommen zum Modul "Suche im TIB-Portal". Hier lernen Sie, was Sie alles in unserem Suchportal finden können und Sie bekommen Tipps, wie Sie es am besten finden.

## Anmeldung im TIB-Portal
In unserem ersten Video zeigen wir Ihnen, warum es sinnvoll ist, sich in unserem TIB-Portal anzumelden, bevor Sie mit Ihrer Suche beginnen.

[Anmeldung im TIB-Portal](https://h5p.org/node/558828)

<iframe src="https://h5p.org/h5p/embed/558828" width="1091" height="742" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://h5p.org/sites/all/modules/h5p/library/js/h5p-resizer.js" charset="UTF-8"></script>


* Welche Titel finde ich im TIB-Portal?
* Wie finde ich ein bestimmtes Buch?
* Wo finde ich Zeitschriften?
* Wie suche ich nach Normen?

